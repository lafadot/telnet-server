import { Socket } from "net";
import {EventEmitter} from "events";
import { EOL } from "os";

/*
Client class impersonates a remote client connection
*/
export class Client{

    private buffer:string[]=[];
    private eventEmitter = new EventEmitter();
    //last message sent from remote client
    private _lastSentMessage:string;
    get lastSentMessage(): string {
        return this._lastSentMessage;
    }
    //last message received from any other remote clients
    private _lastReceivedMessage:string;
    get lastReceivedMessage():string{
        return this._lastReceivedMessage
    }
    constructor(public socket:Socket, public id:string){

        //sanity checks
        if(!socket){
            throw new Error("Socket can't be null");
        }
        if(!id){
            throw new Error("client id can't be empty");
        }
        
        //binding to socket "data" message
        socket.on('data',data=>this.handleData(data))
        //binding to socket "close" message
        socket.on('close',has_error=>this.handleClose(has_error))

        socket.on("error", error=>{console.log(this.id + ": " + error)});
        
    }

    handleData(data:Buffer){
        //utf8 decoding received buffer
        let msg:string = data.toString("utf8");

        //as some telnet clients send data on each single keystroke
        //a check to end of line is added

        //if got an end of line 
        //WARN: tested only on Win env
        if(msg.endsWith(EOL)){
            //push it on message buffer
            this.buffer.push(msg);
            //rebuild the entire message
            this._lastSentMessage= this.buffer.join("");
            //emit "message" event with the final message
            this.eventEmitter.emit('message',this._lastSentMessage);
            //clean the message buffer for a new message
            this.buffer=[];
        }
        else{
            //save to the message buffer
            this.buffer.push(msg);
        }
    }

    //socket "close" message handler
    //emits the same event to listener
    handleClose(has_error:boolean){
        this.eventEmitter.emit('close', this)

    }

    //sending data to remote connected client throgh relative socket
    sendMessage(message:string){
        this._lastReceivedMessage=message;
        this.socket.write(this._lastReceivedMessage);
    }

    //event handlers submission
    on(event:string,handler){
        this.eventEmitter.on(event, handler);
    }
}