import { Listener } from "./listener";
import { createServer} from "net";

const listener = new Listener(createServer());
listener.listen(10000)