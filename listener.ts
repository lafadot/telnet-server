import { Socket,createServer,Server } from "net";
import { Client } from "./client";

const EOL = '\r\n';

/*
Listener class is responsible of listening for incoming connections and manage message forwarding between clients
*/
export class Listener{
    clients:Client[]= [];

    constructor(public server:Server){
        this.server.on('connection', (socket)=> this.onConnection(socket));

    }

    //socket connection event handler
    onConnection(socket:Socket){
            //a new Client instance is saved on a list
            const client = this.saveClient(socket);
            //sending welcome message
            client.sendMessage("server >> welcome!"+ EOL);
            //broadcast all connected clients about a new client
            this.broadcast(client,`client ${client.id} connected`+ EOL)
            //binding to client message event
            client.on('message', (message:string)=>{
               this.onClientMessage(client,message)
            })
            //binding to client close event
            client.on('close', (cl)=> this.onClientClose(cl))
            return client;
    }

    //client "message" event handler
    onClientMessage(client:Client, message:string){
        //forward client message to other clients
        this.broadcast(client,message);
    }

    //client "close" event handler
    onClientClose(cl:Client){
            
            //remove client from clients list
            this.removeClient(cl.id);
            //notify clients about disconnection
            this.broadcast("server", `client ${cl.id} disconnected`+ EOL)
    }
    
    //create a new Client instance starting from an incoming socket
    saveClient(socket:Socket){
        //generating a client id
        const nick = `${socket.remoteAddress}:${socket.remotePort}`;
        //creating Client based on socket and id
        const client = new Client(socket, nick);
        //adding client to connected clients list
        this.clients.push(client)
        return client;
    }

    //client "close" message handler
    removeClient(clientId:string){
        //removing client from connected clients list
        this.clients = this.clients.filter(cl=>cl.id!=clientId);
    }

    //broadcast a message from a client to other connected ones
    broadcast(sender:Client | string, message:string){

    //check if sender is a client or the server itself, identified by just a string
    let senderId:string;
    if (sender instanceof Client){
        senderId = sender.id;
    }
    else{
        senderId = sender;
    }

    const msg = `${senderId} >> ${message}`;
        
    //loop all clients but the sender one
    this.clients.forEach((cl:Client)=>{
        if (cl.id != senderId)
            cl.sendMessage(msg);
    })
    }

    //entry point for start listening incoming connections
    listen(port:number,host?:string){
        
        return this.server.listen(port || 3000,host || '127.0.0.1')
    }
}