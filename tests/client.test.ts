import { ImportMock, MockManager } from 'ts-mock-imports';
import *  as netModule from 'net';
import {Client} from '../client';
import { expect } from 'chai';
import { EOL } from 'os';
var sinon = require('sinon');

describe('client class', function () {
  let socketManager: MockManager<netModule.Socket>;
  let socket;
  let client:Client;
  beforeEach("mock out socket",function(){
    socketManager = ImportMock.mockClass(netModule,"Socket");
    socket= socketManager.getMockInstance()
    socketManager.mock("on",{})
    socketManager.mock("write",(msg:string)=>{return msg})
    client = new Client(socket, 'test');
  })

    
  it('client should throw error if null socket', function () {
    expect(()=>new Client(null, "test")).to.throw("Socket can't be null")
  });

  it('client should throw error if null id', function () {
    expect(()=>new Client(socket, null)).to.throw("client id can't be empty")
  });

  it('client socket should not be null', function () {
    expect(()=>new Client(socket, null)).to.throw("client id can't be empty")
  });

  it('client should send message only on new line',function(){
    var spy = sinon.spy()

    client.on('message', spy)
    client.handleData(Buffer.from("test"+EOL, "utf8"));
    sinon.assert.calledOnce(spy);
  })

  it('client should NOT send message until new line',function(){
    var spy = sinon.spy()
    client.on('message', spy)
    client.handleData(Buffer.from("test", "utf8"));
    sinon.assert.callCount(spy,0);
  })


  it('client should emit "close" event when socket disconnects',function(){
    var spy = sinon.spy()

    client.on('close', spy)
    client.handleClose(false);
    sinon.assert.calledOnce(spy);
  })

  

  afterEach('restore socket', function () {
    socketManager.restore();
  });

});