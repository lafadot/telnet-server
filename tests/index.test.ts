import { ImportMock, MockManager } from 'ts-mock-imports';
import *  as netModule from 'net';
import {Client} from '../client';
import {Listener} from '../listener';
import { expect } from 'chai';
import { EOL } from 'os';
var sinon = require('sinon');

describe("socket listener", function(){
    let socketManager: MockManager<netModule.Socket>;
    let listener:Listener;
    
    before('starting listener',function(){
        listener = new Listener(netModule.createServer());
   
        socketManager = ImportMock.mockClass(netModule, 'Socket');
        socketManager.mock("on",{})
        socketManager.mock("write",(msg:string)=>{return msg})
    })

    beforeEach('clear clients',function(){
        listener.clients = [];
    })

    it("should store new Client on incoming connection", function(){
        let socket= socketManager.getMockInstance()
        const prevclientCount=listener.clients.length;
        listener.onConnection(socket);
        expect(listener.clients.length).equal(prevclientCount+1);
    })

    it("should send welcome message to new clients", function(){
        let socket = socketManager.getMockInstance();
        let client1:Client = listener.onConnection(socket);
        expect(client1.lastReceivedMessage).equal("server >> welcome!"+EOL)
    })

    it("should forward message from client1 to client2", function(){
        
        let socket1 = socketManager.getMockInstance();
        let socket2 = socketManager.getMockInstance();
        let client1:Client = listener.onConnection(socket1);
        client1.id = "client1";
        let client2:Client = listener.onConnection(socket2);
        client2.id="client2";
        client1.handleData(Buffer.from("hello from client1"+EOL));
        expect(client2.lastReceivedMessage).contains("hello from client1");
    })

    it("should clear client on disconnection", function(){
        
        let socket1 = socketManager.getMockInstance();
        let client1:Client = listener.onConnection(socket1);
        client1.handleClose(false);
        expect(listener.clients.length).equals(0);
    })

    
    
    after('restore server', function(){
        socketManager.restore();
    })



})